package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.adapter.CategoryAdapter
import et.edu.aait.student.planetsimulation.dinero.adapter.OnCategoryClickListener
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentCategoryBinding
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import et.edu.aait.student.planetsimulation.dinero.viewmodel.CategoryViewModel
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel

class CategoryFragment : Fragment() {

    lateinit var categoryViewModel: CategoryViewModel
    lateinit var userViewModel: UserViewModel
    lateinit var loggedInUser:User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding: FragmentCategoryBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_category, container, false)
        categoryViewModel =  ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        categoryViewModel.setNormalUser()
        userViewModel.getUser().observe(this,androidx.lifecycle.Observer { user->
            loggedInUser=user
            if(loggedInUser?.role ==1){
                Log.d("inside check ",loggedInUser?.role.toString())
                categoryViewModel.setAdmin()
            }
        })




        binding.categoryViewModel = categoryViewModel

        val categoryAdapter = CategoryAdapter(object:OnCategoryClickListener{
            override fun onCategoryClick(category: Category) {
                val args = Bundle()
                args.putSerializable("category",category)
                if (loggedInUser?.role == 1){
                    findNavController().navigate(R.id.room_list_des,args)
                }
                else if (loggedInUser.role == 2){
                    findNavController().navigate(R.id.gameplay_des,args)
                }

            }

        })
        binding.categoryListRecyclerView.adapter = categoryAdapter

        if(Utility.checkConnection(context)){
            categoryViewModel.getCategories()
            categoryViewModel.getResponses.observe(this, androidx.lifecycle.Observer {response->
                response?.run {
                    categoryAdapter.submitList(response)
                }
            })
        }

        categoryViewModel.addCategoryResponse.observe(this, Observer { bool->
            if(bool){
                findNavController().navigate(R.id.add_category_des,null)
            }
        })
        binding.executePendingBindings()

        binding.setLifecycleOwner{ lifecycle }

        return binding.root
    }


}