package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User):Long

    @Update
    fun updateUser(user: User?): Long

    @Delete
    fun deleteUser(user: User?): Long

    @Query("SELECT * FROM user WHERE user.id = :user_id LIMIT 1")
    fun getUserById(user_id: Long) : LiveData<User>

    @Query("SELECT * FROM user LIMIT 1")
    fun getUser():LiveData<User>
}