package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.Question

@Dao
interface QuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuestion(question: Question):Long

    @Update
    fun updateQuestion(question: Question):Int

    @Delete
    fun deleteQuestion(question: Question):Int

    @Query("SELECT * FROM questions WHERE questions.id= :id LIMIT 1")
    fun getQuestionById(id:Long): LiveData<Question>

    @Query("SELECT * FROM questions")
    fun getQuestions():LiveData<List<Question>>
}