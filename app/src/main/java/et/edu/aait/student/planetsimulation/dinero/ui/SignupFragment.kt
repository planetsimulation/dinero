package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentSignupBinding
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel

class SignupFragment: Fragment() {

    lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentSignupBinding>(inflater,
            R.layout.fragment_signup, container, false)

        binding.userViewModel = userViewModel

        return binding.root
    }



}