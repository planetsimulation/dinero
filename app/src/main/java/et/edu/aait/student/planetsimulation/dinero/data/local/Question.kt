package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.Choice
import java.util.*

@Entity(
    tableName = "questions",
    foreignKeys = [
        ForeignKey(entity = GameRoom::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("room_id"),
        onDelete = ForeignKey.CASCADE),
        ForeignKey(entity = Choice::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("choice_id"),
            onDelete = ForeignKey.CASCADE),
        ForeignKey(entity = Answer::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("answer_id"),
            onDelete = ForeignKey.CASCADE)
    ]
)


data class Question (
    @PrimaryKey @ColumnInfo(name="id") var id:Long,
    @ColumnInfo(name = "created_at") val createdAt: Date?,
    @ColumnInfo(name= "content") val content:String,
    @ColumnInfo(name="xp") val xp:Int,
    @ColumnInfo(name="room_id") val roomId:Long,
    @ColumnInfo(name="choice_id") val choiceId:Long,
    @ColumnInfo(name="answer_id") val answerId:Long
)