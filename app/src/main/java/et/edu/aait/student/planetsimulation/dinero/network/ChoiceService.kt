package et.edu.aait.student.planetsimulation.dinero.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.ChoiceRetro
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

interface ChoiceService{

    @GET("choice/{id}")
    fun getChoiceAsync(@Path("id")id:Long): Deferred<Response<ChoiceRetro>>

    @GET("choice/all")
    fun getChoicesAsync(): Deferred<Response<List<ChoiceRetro>>>

    @POST("choice")
    fun addChoiceAsync(@Body choiceRetro: ChoiceRetro): Deferred<Response<ChoiceRetro>>

    @PUT("choice/{id}")
    fun updateChoiceAsync(@Path("id") id :Long,@Body choiceRetro: ChoiceRetro):Deferred<Response<ChoiceRetro>>

    @DELETE("choice/{id}")
    fun deleteChoiceAsync(@Path("id") id:Long):Deferred<Response<Void>>

    companion object{
        val baseUrl=Utility.BASE_URL

        fun getInstance():ChoiceService{

            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(QuestionService.baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ChoiceService::class.java)
        }
    }
}
