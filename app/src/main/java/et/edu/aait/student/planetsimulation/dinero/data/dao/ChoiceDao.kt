package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.Choice

@Dao
interface ChoiceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChoice(choice: Choice):Long

    @Update
    fun updateChoice(choice: Choice):Int

    @Delete
    fun deleteChoice(choice: Choice):Int

    @Query("SELECT * FROM choices WHERE choices.id= :id LIMIT 1 ")
    fun getChoiceById(id:Long):LiveData<Choice>
}