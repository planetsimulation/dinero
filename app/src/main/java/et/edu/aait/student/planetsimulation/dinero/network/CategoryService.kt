package et.edu.aait.student.planetsimulation.dinero.network

import androidx.room.Update
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

interface CategoryService {

    @GET("category/{id}")
    fun getCategoryAsync(@Path("id")id:Long): Deferred<Response<CategoryRetro>>

    @GET("category/all")
    fun getCategoriesAsync():Deferred<Response<List<CategoryRetro>>>

    @POST("category")
    fun addCategoryAsync(@Body categoryRetro: CategoryRetro): Deferred<Response<CategoryRetro>>

    @PUT("category/{id}")
    fun updateCategoryAsync(@Path("id") id: Long, @Body categoryRetro: CategoryRetro):Deferred<Response<CategoryRetro>>

    @DELETE("category/{id}")
    fun deleteCategoryAsync(@Path("id") id:Long):Deferred<Response<Void>>


    companion object{
        val baseUrl = Utility.BASE_URL
        fun getInstance(): CategoryService{
            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(CategoryService::class.java)
        }
    }
}