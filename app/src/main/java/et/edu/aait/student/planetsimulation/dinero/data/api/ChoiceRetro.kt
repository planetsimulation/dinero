package et.edu.aait.student.planetsimulation.dinero.data.api

import et.edu.aait.student.planetsimulation.dinero.data.local.Choice
import java.io.Serializable
import java.util.*

data class ChoiceRetro(
    val id:Long,
    val firstChoice:String,
    val secondChoice:String,
    val thirdChoice:String,
    val fourthChoice:String
):Serializable{

    fun convertToChoice():Choice{

        val choice  = Choice(this.id, this.firstChoice, this.secondChoice,this.thirdChoice,this.fourthChoice)
        return choice
    }
}

