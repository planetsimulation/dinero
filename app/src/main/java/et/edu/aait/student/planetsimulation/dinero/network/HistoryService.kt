package et.edu.aait.student.planetsimulation.dinero.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.GameRoomRetro
import et.edu.aait.student.planetsimulation.dinero.data.api.HistoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

interface HistoryService {

    @POST("history/add")
    fun insertHistoryAsync(@Body history: History): Deferred<Response<List<History>>>

    @GET("history")
    fun getAllHistoryByIdAsync() : Deferred<Response<List<HistoryRetro>>>

    @PUT("history/{id}")
    fun updateHistoryAsync(@Path("id") id:Long, @Body historyRetro: HistoryRetro):Deferred<Response<GameRoomRetro>>

    @DELETE("history{id}")
    fun deleteHistoryAsync(@Path("id") id:Long): Deferred<Response<Void>>


    companion object{
        val baseUrl = Utility.BASE_URL
        fun getInstance(): HistoryService{
            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(HistoryService::class.java)
        }
    }
}