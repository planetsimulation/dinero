package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.ChoiceRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Choice
import et.edu.aait.student.planetsimulation.dinero.network.ChoiceService
import et.edu.aait.student.planetsimulation.dinero.repository.ChoiceRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class ChoiceViewModel(application: Application):AndroidViewModel(application) {

    var choiceService: ChoiceService
    var choiceRepository: ChoiceRepository

    init {
        val database= DineroDatabase.getDatabase(application)
        val choiceDao=database.choiceDao()

        choiceService= ChoiceService.getInstance()
        choiceRepository= ChoiceRepository(application,choiceDao,choiceService)
    }

    private val _getResponse = MutableLiveData<Choice>()
    private val _getResponses= MutableLiveData<List<Choice>>()
    private val _insertResponse = MutableLiveData<Choice>()
    private val _updateResponse = MutableLiveData<Choice>()

    val getResponse:LiveData<Choice>
        get() = _getResponse

    val getResponses:LiveData<List<Choice>>
        get() = _getResponses

    val insertResponse:LiveData<Choice>
        get() = _insertResponse

    val updateResponse:LiveData<Choice>
        get() = _updateResponse


    fun insertChoice(choiceRetro: ChoiceRetro) = viewModelScope.launch{
        choiceRepository.insertChoiceNet(choiceRetro).observeForever{
            _insertResponse.postValue(it)
        }
    }

    fun getChoiceById(choiceId: Long) = viewModelScope.launch {
        choiceRepository.getChoiceByIdNet(choiceId).observeForever{
            _getResponse.postValue(it)
        }
    }

    fun updateChoice(choice: ChoiceRetro) = viewModelScope.launch{
        choiceRepository.updateChoiceNet(choice).observeForever{
            _updateResponse.postValue(it)
        }
    }

    fun deleteChoice(choice: ChoiceRetro)= viewModelScope.launch{
        choiceRepository.deleteChoiceNet(choice)
    }
}