package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.api.QuestionRetro
import et.edu.aait.student.planetsimulation.dinero.data.dao.QuestionDao
import et.edu.aait.student.planetsimulation.dinero.data.local.Question
import et.edu.aait.student.planetsimulation.dinero.network.QuestionService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response

class QuestionRepository(val application: Application,val questionDao: QuestionDao, val questionService: QuestionService) {
    
    //Do web calls if network available and then do local caching
    suspend fun getQuestionByIdNet(questionId: Long): LiveData<Question> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){

                val response = questionService.getQuestionAsync(questionId).await()
                val question = response.body()
                if(question != null){
                    questionDao.insertQuestion(question.convertToQuestion())
                }
            }
            return@withContext questionDao.getQuestionById(questionId)
        }

    suspend fun getQuestionNet():LiveData<List<Question>> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                val response=questionService.getQuestionsAsync().await()
                val categories = response.body()
                if (categories != null) {
                    categories.forEach {
                            question-> questionDao.insertQuestion(question.convertToQuestion())

                    }

                }
            }
            return@withContext questionDao.getQuestions()
        }

    suspend fun getQuestions(): Response<List<QuestionRetro>> = withContext(Dispatchers.IO){
        questionService.getQuestionsAsync().await()
    }

    suspend fun insertQuestionNet(questionRetro: QuestionRetro):LiveData<Question> =
        withContext(Dispatchers.IO){
            Log.d("question about",questionRetro.toString())
            if(Utility.checkConnection(application)){
                questionService.addQuestionAsync(questionRetro).await()
            }

            //questionDao.insertQuestion(questionRetro.convertToQuestion())
            return@withContext questionDao.getQuestionById(questionRetro.id)

        }

    suspend fun updateQuestionNet(questionRetro: QuestionRetro):LiveData<Question> =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                questionService.updateQuestionAsync(questionRetro.id,questionRetro)
            }

            questionDao.updateQuestion(questionRetro.convertToQuestion())

            return@withContext questionDao.getQuestionById(questionRetro.id)
        }

    suspend fun deleteQuestionNet(questionRetro: QuestionRetro) =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                questionService.deleteQuestionAsync(questionRetro.id)
            }

            questionDao.deleteQuestion(questionRetro.convertToQuestion())
        }
}