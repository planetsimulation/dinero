package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.api.HistoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.dao.HistoryDao
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.network.HistoryService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HistoryRepository (val application: Application, val historyDao: HistoryDao, val historyService: HistoryService) {


    suspend fun getHistories(user: User): LiveData<List<History>> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                val response=historyService.getAllHistoryByIdAsync().await()
                val histories = response.body()
                if (histories != null) {

                    histories.forEach {
                            history-> historyDao.insertHistory(history.convertToHistory())
                    }
                }
            }

            return@withContext historyDao.getHistoriesByUserId(user.id)
        }

    suspend fun insertHistoryNet(history: HistoryRetro):LiveData<History> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                val response = historyService.insertHistoryAsync(history.convertToHistory()).await()
            }

            historyDao.insertHistory(history.convertToHistory())
            return@withContext historyDao.getHistoryById(history.id)

        }

    suspend fun updateHistoryNet(historyRetro: HistoryRetro):LiveData<History> =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                historyService.updateHistoryAsync(historyRetro.id,historyRetro)
            }

            historyDao.updateHistory(historyRetro.convertToHistory())

            return@withContext historyDao.getHistoryById(historyRetro.id)
        }

    suspend fun deleteHistoryNet(historyRetro: HistoryRetro) =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                historyService.deleteHistoryAsync(historyRetro.id)
            }

            historyDao.deleteHistory(historyRetro.convertToHistory())
        }
    
    

}