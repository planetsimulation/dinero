package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "history",
    foreignKeys = arrayOf(
        ForeignKey(entity = User::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("user_id"),
            onDelete = ForeignKey.CASCADE)
    )
)
data class History (
    @PrimaryKey @ColumnInfo(name = "id") val id : Long,
    @ColumnInfo(name = "played_at") val playedAt: Date,
    @ColumnInfo(name = "credits") val credits: Int,
    @ColumnInfo(name = "points") val points: Int,
    @ColumnInfo(name = "total_points") val totalPoints: Int,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "user_id") val userId: Long
){

    var wins = (1).toString()
    var loses = (0).toString()
    var coins = (1000).toString()
    var idStr = id.toString()
}

//
