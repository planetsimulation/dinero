package et.edu.aait.student.planetsimulation.dinero.network

import androidx.room.Update
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.QuestionRetro
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

interface QuestionService {

    @GET("question/{id}")
    fun getQuestionAsync(@Path("id")id:Long):Deferred<Response<QuestionRetro>>

    @GET("question/all")
    fun getQuestionsAsync():Deferred<Response<List<QuestionRetro>>>

    @GET("question/{category}")
    fun getQuestionsByCategoryAsync(@Path("category") category: String): Deferred<Response<List<QuestionRetro>>>

    @POST("question")
    fun addQuestionAsync(@Body questionRetro: QuestionRetro):Deferred<Response<QuestionRetro>>

    @PUT("question/{id}")
    fun updateQuestionAsync(@Path("id") id:Long,@Body questionRetro: QuestionRetro):Deferred<Response<QuestionRetro>>

    @DELETE("question/{id}")
    fun deleteQuestionAsync(@Path("id") id:Long):Deferred<Response<Void>>

    companion object{
        val baseUrl= Utility.BASE_URL
        fun getInstance():QuestionService{

            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()

            val retrofit:Retrofit= Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(QuestionService::class.java)

        }
    }
}