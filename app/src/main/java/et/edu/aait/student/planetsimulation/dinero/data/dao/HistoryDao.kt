package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.History

@Dao
interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHistory(history: History)

    @Query("SELECT * from history where history.user_id = :id")
    fun getHistoriesByUserId(id: Long): LiveData<List<History>>

    @Query("SELECT * from history where history.id = :id")
    fun getHistoryById(id: Long): LiveData<History>

    @Update
    fun updateHistory(history: History):Int

    @Delete
    fun deleteHistory(history: History):Int
}