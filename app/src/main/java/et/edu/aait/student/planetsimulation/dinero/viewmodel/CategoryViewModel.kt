package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import androidx.databinding.Bindable
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.network.CategoryService
import et.edu.aait.student.planetsimulation.dinero.repository.CategoryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import android.util.Log
import androidx.lifecycle.*
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import java.util.*
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry


class CategoryViewModel(application: Application):AndroidViewModel(application),Observable{


    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    var categoryService:CategoryService
    var categoryRepository:CategoryRepository
    var applicationContext:Application


    init {
        val database=DineroDatabase.getDatabase(application)
        val categoryDao=database.categoryDao()

        categoryService= CategoryService.getInstance()
        categoryRepository= CategoryRepository(application,categoryDao,categoryService)
        applicationContext=application
    }

    private val _getResponse = MutableLiveData<Category>()
    private val _getResponses= MutableLiveData<List<Category>>()
    private val _insertResponse = MutableLiveData<Category>()
    private val _updateResponse = MutableLiveData<Category>()
    private val _addCategoryResponse = MutableLiveData<Boolean>()

    private val _isAdmin = MutableLiveData<Boolean>()

    @Bindable
    val categoryName = MutableLiveData<String>()

    @Bindable
    val categoryDescription= MutableLiveData<String>()

    val getIsAdmin:MutableLiveData<Boolean>
        get() = _isAdmin

    @Bindable
    fun getAd():Boolean{
        return getIsAdmin.value!!
    }

    val insertResponse: MutableLiveData<Category>
        get() = _insertResponse

    val getResponse:LiveData<Category>
        get() = _getResponse

    val getResponses:MutableLiveData<List<Category>>
        get() = _getResponses

    val updateResponse:MutableLiveData<Category>
        get() = _updateResponse

    val addCategoryResponse:LiveData<Boolean>
        get() = _addCategoryResponse



    fun getCategories()= viewModelScope.launch {
        categoryRepository.getCategoriesNet().observeForever{
            _getResponses.postValue(it)
        }
    }

    fun insertCategory(categoryRetro: CategoryRetro) = viewModelScope.launch{
        categoryRepository.insertCategoryNet(categoryRetro).observeForever{
            _insertResponse.postValue(it)
        }
    }

    fun getCategoryById(categoryId: Long) = viewModelScope.launch {
        categoryRepository.getCategoryByIdNet(categoryId).observeForever{
            _getResponse.postValue(it)
        }
    }

    fun updateCategory(category: CategoryRetro) = viewModelScope.launch{
        categoryRepository.updateCategoryNet(category).observeForever{
            _updateResponse.postValue(it)
        }
    }

    fun deleteCategory(category: CategoryRetro)= viewModelScope.launch{
        categoryRepository.deleteCategoryNet(category)
    }

    fun onAddCategoryButtonClick(){

        val category= readCategory()
        insertCategory(category)
        insertResponse.observeForever(androidx.lifecycle.Observer { response->
            response?.run{
                Log.d("inserted category",this.toString())
            }
        })
        Log.d("add_category",this.getResponse.toString())
    }

    fun readCategory()=CategoryRetro(
        0,
        Date(),
        categoryName.value.toString(),
        categoryDescription.value.toString()
    )

    fun setAdmin(){
        _isAdmin.value=true
    }

    fun setNormalUser(){
        _isAdmin.value=false
    }

    fun onAddFabClick(){
        _addCategoryResponse.value = true
    }




}