package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.data.api.*
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentAddQuestionBinding
import et.edu.aait.student.planetsimulation.dinero.viewmodel.*
import kotlinx.android.synthetic.main.fragment_add_question.view.*
import java.util.*

class AddQuestionFragment:Fragment() {



    lateinit var categoryViewModel: CategoryViewModel

    lateinit var addQuestionViewModel: AddQuestionViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //val view= inflater.inflate(R.layout.fragment_add_question,container,false)
        val binding = DataBindingUtil.inflate<FragmentAddQuestionBinding>(
            inflater,
            R.layout.fragment_add_question,
            container,
            false
        )

        val category = arguments?.getSerializable("category") as Category
        val gameRoom = arguments?.getSerializable("game") as GameRoom



        addQuestionViewModel = ViewModelProviders.of(this).get(AddQuestionViewModel::class.java)
        addQuestionViewModel.setCategory(category)
        addQuestionViewModel.setGameRoom(gameRoom)
        binding.addQuestionViewModel= addQuestionViewModel

        addQuestionViewModel.addQuestionResponse.observe(this,androidx.lifecycle.Observer {

            if(it){
                findNavController().navigate(R.id.category_des)
            }
        })

        return binding.root
    }


}

