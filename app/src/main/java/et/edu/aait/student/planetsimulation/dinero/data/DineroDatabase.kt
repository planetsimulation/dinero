package et.edu.aait.student.planetsimulation.dinero.data


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import et.edu.aait.student.planetsimulation.dinero.data.dao.*
import et.edu.aait.student.planetsimulation.dinero.data.local.*

@Database(entities = arrayOf(User::class, Category::class, Question::class, Choice::class, Answer::class, GameRoom::class, History::class), version = 7)
@TypeConverters(DateConverter::class)
abstract class DineroDatabase: RoomDatabase() {
    // function that return Dao class

    abstract fun userDao(): UserDao
    abstract fun categoryDao(): CategoryDao
    abstract fun questionDao(): QuestionDao
    abstract fun choiceDao(): ChoiceDao
    abstract fun answerDao(): AnswerDao
    abstract fun roomDao(): GameRoomDao
    abstract fun historyDao(): HistoryDao


    companion object {

        @Volatile
        private var INSTANCE: DineroDatabase? = null

        fun getDatabase(context: Context): DineroDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DineroDatabase::class.java, "dinero"
                ).fallbackToDestructiveMigration().build()

                INSTANCE = instance
                return instance
            }

        }
    }
}
