package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey @ColumnInfo ( name = "id" ) var id: Long,
    @ColumnInfo(name = "created_at") val createdAt:Date?,
    @ColumnInfo(name = "name") val name:String,
    @ColumnInfo(name = "details") val details:String):Serializable