package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "user")
data class User (
    @PrimaryKey @ColumnInfo ( name = "id" ) val id: Long,
    @ColumnInfo(name = "created_at") val createdAt:Date?,
    @ColumnInfo(name = "email") val email:String,
    @ColumnInfo(name = "first_name") val firstName:String,
    @ColumnInfo(name = "last_name") val lastName:String,
    @ColumnInfo(name = "password") val password:String,
    @ColumnInfo(name = "xp_earned") val xp: Int,
    @ColumnInfo(name = "credit") var credit: Int,
    @ColumnInfo(name= "role") var role:Int
): Serializable