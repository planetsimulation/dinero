package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.api.ChoiceRetro
import et.edu.aait.student.planetsimulation.dinero.data.dao.ChoiceDao
import et.edu.aait.student.planetsimulation.dinero.data.local.Choice
import et.edu.aait.student.planetsimulation.dinero.network.ChoiceService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response

class ChoiceRepository(val application: Application,val choiceDao: ChoiceDao, val choiceService: ChoiceService) {

    //Do web calls if network available and then do local caching
    suspend fun getChoiceByIdNet(choiceId: Long): LiveData<Choice> =
        withContext(Dispatchers.IO){
            if (Utility.checkConnection(application)){

                val response = choiceService.getChoiceAsync(choiceId).await()
                val choice =  response.body()
                if (choice != null) {
                    choiceDao.insertChoice(choice.convertToChoice())
                }
            }
            return@withContext choiceDao.getChoiceById(choiceId)
        }

    suspend fun insertChoiceNet(choiceRetro: ChoiceRetro): LiveData<Choice> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                choiceService.addChoiceAsync(choiceRetro).await()
            }

            choiceDao.insertChoice(choiceRetro.convertToChoice())
            return@withContext choiceDao.getChoiceById(choiceRetro.id)
        }

    suspend fun updateChoiceNet(choiceRetro: ChoiceRetro):LiveData<Choice> =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                choiceService.updateChoiceAsync(choiceRetro.id,choiceRetro)
            }

            choiceDao.updateChoice(choiceRetro.convertToChoice())

            return@withContext choiceDao.getChoiceById(choiceRetro.id)
        }

    suspend fun deleteChoiceNet(choiceRetro: ChoiceRetro) =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                choiceService.deleteChoiceAsync(choiceRetro.id)
            }

            choiceDao.deleteChoice(choiceRetro.convertToChoice())
        }
}