package et.edu.aait.student.planetsimulation.dinero.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.databinding.GameRecyclerViewItemBinding

class GameAdapter(onGameClickListener:OnGameClickListener):ListAdapter<GameRoom, GameAdapter.ViewHolder>(GameDiffCallBack()){

    private val onClick= onGameClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item,onClick)
    }



    class ViewHolder private constructor(val binding: GameRecyclerViewItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(item: GameRoom, onClick:OnGameClickListener){
            binding.gameRoom = item
            binding.root.setOnClickListener {
                onClick.onGameClick(item)
            }
            binding.executePendingBindings()
        }

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GameRecyclerViewItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class GameDiffCallBack: DiffUtil.ItemCallback<GameRoom>(){
    override fun areItemsTheSame(oldItem: GameRoom, newItem: GameRoom): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GameRoom, newItem: GameRoom): Boolean {
        return oldItem == newItem
    }

}

interface OnGameClickListener{
    fun onGameClick(game:GameRoom)
}