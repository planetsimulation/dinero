package et.edu.aait.student.planetsimulation.dinero.ui

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.adapter.HistoryAdapter
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentAccountBinding
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import et.edu.aait.student.planetsimulation.dinero.viewmodel.HistoryViewModel
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel
import java.util.*

class Account : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var historyViewModel: HistoryViewModel
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAccountBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_account, container, false)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel::class.java)


        binding.userViewModel = userViewModel
        binding.userHistory = History(1, Date(), 1, 1, 1, "WON", 1)

        userViewModel.getUser().observe(viewLifecycleOwner, Observer {
            binding.user = it
            val historyAdapter = HistoryAdapter(historyViewModel)
            binding.historyRecyclerView.adapter = historyAdapter

            historyViewModel.getHistories(it)
            historyViewModel.getResponses.observe(this, Observer {list ->
                historyAdapter.submitList(list)
            })
        })

        userViewModel.logoutResponse.observe(this, Observer { bool->
            if(bool){
                findNavController().navigate(R.id.loginFragment,null)
            }
        })

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}