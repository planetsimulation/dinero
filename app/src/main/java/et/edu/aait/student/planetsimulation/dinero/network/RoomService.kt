package et.edu.aait.student.planetsimulation.dinero.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.GameRoomRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

interface RoomService {
    @GET("room/all")
    fun getAllRoomsAsync() : Deferred<Response<List<GameRoomRetro>>>

    @GET("room/active")
    fun getAllActiveRoomsAsync(): Deferred<Response<List<GameRoomRetro>>>

    @GET("room/specific")
    fun getRoomsByCategoryAsync(@Query("category_id") category:Long):Deferred<Response<List<GameRoomRetro>>>

    @POST("room/add")
    fun addRoomAsync(@Body gameRoomRetro: GameRoomRetro):Deferred<Response<GameRoomRetro>>

    @PUT("room/{id}")
    fun updateRoomAsync(@Path("id") id:Long,@Body gameRoomRetro: GameRoomRetro):Deferred<Response<GameRoomRetro>>

    @DELETE("room/{id}")
    fun deleteRoomAsync(@Path("id") id:Long):Deferred<Response<Void>>

    @GET("room/{id}")
    fun getRoomById(@Path("id") id:Long ):Deferred<Response<GameRoomRetro>>

    companion object{
        val baseUrl = Utility.BASE_URL
        fun getInstance(): RoomService{
            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(RoomService::class.java)
        }
    }
}