package et.edu.aait.student.planetsimulation.dinero.data.api

import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import java.io.Serializable
import java.util.*

data class GameRoomRetro(val id: Long, val createdAt: Date,
                         val type: GameRoom.Type, val category: Category,
                         val price: Int, val roomName: String): Serializable{

    fun convertToGameRoom():GameRoom{

        return GameRoom(this.id,this.createdAt,this.type.ordinal,category.id,price,roomName)

    }
}