package et.edu.aait.student.planetsimulation.dinero.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.fragment_payment.view.*
import java.lang.NumberFormatException


class payment : Fragment() {
//    private var listener: PaymentActions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payment, container, false)
//        val binding: FragmentPaymentBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_payment,
//                                                                              container, false)
        val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
//        binding.userViewModel = ViewModelProviders.of(this)
//        binding.userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
//        binding.setLifecycleOwner { lifecycle }
//
        view.credit_buy_btn.setOnClickListener {
            Toast.makeText(context, "Payment Being Made", Toast.LENGTH_SHORT).show()
            val i = Intent(Intent.ACTION_CALL)
            val code = "*804#"
            i.data = Uri.parse("tel:" + code.replace("#", Uri.encode("#")))

//            if (i.resolveActivity(activity?.packageManager) != null) {
//                  startActivity(i)
//            }
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.CALL_PHONE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                startActivity(i)
            } else {
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 1)
//                startActivity(i)
            }

            try {
                val x = credit_edit_text.text.toString().toInt()
                userViewModel.getUser().observe(this, Observer {
                    userViewModel.getUserByEmailNet(it.email)
                    userViewModel.getResponse.observe(viewLifecycleOwner, Observer {
                        response -> response.body()?.run {
                            this.credit += x
                            userViewModel.updateUserNetAsync(this)
                        }
                    })
                })
            }
            catch (e: NumberFormatException){
                Toast.makeText(context, "Wrong Input for Credit", Toast.LENGTH_SHORT).show()
            }
        }

        return view
    }

}
