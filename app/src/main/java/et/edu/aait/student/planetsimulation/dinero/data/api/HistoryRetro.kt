package et.edu.aait.student.planetsimulation.dinero.data.api

import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import java.util.*

data class HistoryRetro(
    val id: Long,
    val credit: Int,
    val playedAt: Date,
    val points: Int,
    val totalPoints: Int,
    val type: String,
    val user: User?,
    val room: GameRoomRetro
){

    fun convertToHistory(): History {

        return History(id, playedAt, credit, points, totalPoints, type, user!!.id)
    }
}