package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.api.GameRoomRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.data.dao.GameRoomDao
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.network.RoomService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response

class RoomRepository(val application: Application,val roomDao: GameRoomDao, val roomService: RoomService) {


    suspend fun getAllActiveRoomsNet():LiveData<List<GameRoom>> = withContext(Dispatchers.IO){


        if(Utility.checkConnection(application)){

            val response = roomService.getAllActiveRoomsAsync().await()
            val rooms = response.body()
            if (rooms != null) {
                Log.d("inside repo function",rooms.size.toString())
            }
            if(rooms != null){
                rooms.forEach {
                    room->roomDao.insertRoom(room.convertToGameRoom())
                }
            }
        }
        return@withContext roomDao.getRooms()
    }

    suspend fun getRoomsByCategory(category: Category):LiveData<List<GameRoom>> = withContext(Dispatchers.IO){


        if(Utility.checkConnection(application)){

            val response = roomService.getRoomsByCategoryAsync(category.id).await()
            val rooms = response.body()
            if (rooms != null) {
                Log.d("inside repo function",rooms.size.toString())
            }
            if(rooms != null){
                rooms.forEach {
                        room->roomDao.insertRoom(room.convertToGameRoom())
                }
            }
        }
        return@withContext roomDao.getRooms()
    }

    suspend fun insertRoom(gameRoomRetro: GameRoomRetro):LiveData<GameRoom> =
        withContext(Dispatchers.IO){
            val response = roomService.addRoomAsync(gameRoomRetro).await()
            val room = response.body()
            if(room!=null){
                roomDao.insertRoom(room.convertToGameRoom())
            }

            if (room != null) {
                return@withContext roomDao.getRoomById(room.id)
            }
            else{
                return@withContext roomDao.getRoomById(gameRoomRetro.id)
            }


        }




}