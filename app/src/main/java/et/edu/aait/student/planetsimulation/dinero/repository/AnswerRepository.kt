package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.dao.AnswerDao
import et.edu.aait.student.planetsimulation.dinero.data.api.AnswerRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Answer
import et.edu.aait.student.planetsimulation.dinero.network.AnswerService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AnswerRepository(val application: Application,val answerDao: AnswerDao, val answerService: AnswerService) {

    //Do web calls if network available and then do local caching
    suspend fun getAnswerByIdNet(answerId: Long): LiveData<Answer> =
        withContext(Dispatchers.IO){
            if (Utility.checkConnection(application)){

                val response = answerService.getAnswerAsync(answerId).await()
                val answer =  response.body()
                if (answer != null) {
                    answerDao.insertAnswer(answer.convertToAnswer())
                }
            }
        return@withContext answerDao.getAnswerById(answerId)
    }

    suspend fun insertAnswerNet(answerRetro: AnswerRetro): LiveData<Answer> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                answerService.addAnswerAsync(answerRetro).await()
            }

            answerDao.insertAnswer(answerRetro.convertToAnswer())
            return@withContext answerDao.getAnswerById(answerRetro.id)
    }

    suspend fun updateAnswerNet(answerRetro: AnswerRetro):LiveData<Answer> =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                answerService.updateAnswerAsync(answerRetro.id,answerRetro)
            }

            answerDao.updateAnswer(answerRetro.convertToAnswer())

            return@withContext answerDao.getAnswerById(answerRetro.id)
        }

    suspend fun deleteAnswerNet(answerRetro: AnswerRetro) =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                answerService.deleteAnswerAsync(answerRetro.id)
            }

            answerDao.deleteAnswer(answerRetro.convertToAnswer())
        }
}