package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.squareup.moshi.internal.Util
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.data.api.HistoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentGameplayBinding
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import et.edu.aait.student.planetsimulation.dinero.viewmodel.*
import java.util.*

class GamePlayFragment: Fragment() {
    private lateinit var questionViewModel: QuestionViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var historyViewModel: HistoryViewModel
//    private lateinit var answerViewModel: AnswerViewModel
    private lateinit var choice1: Button
    private lateinit var choice2: Button
    private lateinit var choice3: Button
    private lateinit var choice4: Button
    private lateinit var question: TextView
    private lateinit var score: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val view = inflater.inflate(R.layout.fragment_gameplay, container, false)

        val binding: FragmentGameplayBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_gameplay, container, false)

        questionViewModel = ViewModelProviders.of(this).get(QuestionViewModel::class.java)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel::class.java)
//        answerViewModel = ViewModelProviders.of(this).get(AnswerViewModel::class.java)
//
        binding.score = ""+Utility.score
//
        fun setListeners(){
            var chosen: String
            val answer: String = Utility.questionList!![Utility.questionIndex].answer.content
            binding.choice1Value.setOnClickListener {
                chosen = Utility.questionList!![Utility.questionIndex].choice.firstChoice
                nextQuestion(answer, chosen, Utility.questionList!![Utility.questionIndex].xp)
            }
            binding.choice2Value.setOnClickListener {
                chosen = Utility.questionList!![Utility.questionIndex].choice.secondChoice
                nextQuestion(answer, chosen, Utility.questionList!![Utility.questionIndex].xp)
            }
            binding.choice3Value.setOnClickListener {
                chosen = Utility.questionList!![Utility.questionIndex].choice.thirdChoice
                nextQuestion(answer, chosen, Utility.questionList!![Utility.questionIndex].xp)
            }
            binding.choice4Value.setOnClickListener {
                chosen = Utility.questionList!![Utility.questionIndex].choice.fourthChoice
                nextQuestion(answer, chosen, Utility.questionList!![Utility.questionIndex].xp)
            }
        }
//
        questionViewModel.getQuestions()
//
        if(Utility.questionList!!.isEmpty()){
            questionViewModel.getResponses.observe(this, Observer {
                    response -> response?.run {
                this.body()?.run {
                    Utility.questionList = this
                    binding.question = this[Utility.questionIndex]
                    binding.score = ""+Utility.score
                    binding.executePendingBindings()
                    setListeners()
                }

            }
            })
        }
        else{
            // If questions are already loaded
            binding.question = Utility.questionList!![Utility.questionIndex]
            binding.executePendingBindings()
            setListeners()
        }



        return binding.root
    }

    fun nextQuestion(answer: String, chosen: String, xp: Int){
        val bool = answer == chosen
//        Log.d("GAME_PLAY_TAG", "Answer: $bool")
        if (Utility.questionIndex++ < Utility.questionList!!.size - 1){
            // if the questions are not over
            if(answer == chosen){
                Utility.score += xp
            }
            else{
                Utility.wrong = 1
            }
            findNavController().navigate(R.id.gameplay_des)
        }
        else{
            // if the questions are over
            if(answer == chosen){
                Utility.score += xp
            }
            else{
                Utility.wrong = 1
            }

            if(Utility.wrong == 0){
                var x = 0
                userViewModel.getUser().observe(viewLifecycleOwner, Observer {
                    if(x == 0 && Utility.questionList!!.isNotEmpty())  {
                        Log.d("GAME_PLAY_TAG", "HERE + " + Utility.questionList!![0].room.id)
                        historyViewModel.insertHistory(
                            HistoryRetro(
                                0,
                                5,
                                Date(),
                                Utility.score,
                                Utility.questionList!!.size * 10,
                                "WON",
                                it,
                                Utility.questionList!![0].room
                            )
                        )
                        x = 1
                    }

                    Utility.questionList = arrayListOf()
                    Utility.questionIndex = 0
                    Utility.score = 0
                    Utility.wrong = 0
                })
                userViewModel.getResponse.observe(viewLifecycleOwner, Observer {
                    it.body().run {
                        Log.d("GAME_PLAY_TAG", "here")

                    }
                })
                Toast.makeText(context, "Congratulations You have won", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(context, "Sorry you have lost", Toast.LENGTH_SHORT).show()
            }

            findNavController().navigate(R.id.home_des)
        }
    }
}