package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.*
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.adapter.GameRoomAdapter
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.GameRoomRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.network.RoomService
import et.edu.aait.student.planetsimulation.dinero.repository.RoomRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import java.util.*

class RoomViewModel(application: Application): AndroidViewModel(application),Observable {

    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }
    var roomService:RoomService
    var roomRepository:RoomRepository

    init {
        val database = DineroDatabase.getDatabase(application)
        var roomDao = database.roomDao()

        roomService = RoomService.getInstance()
        roomRepository = RoomRepository(application,roomDao, roomService)
    }

    private val _getResponse = MutableLiveData<Response<GameRoomRetro>>()
    private val _getResponses= MutableLiveData<List<GameRoom>>()
    private val _insertResponse = MutableLiveData<GameRoom>()
    private val _addRoomResponse = MutableLiveData<Boolean>()
    private val _chosenCategory = MutableLiveData<Category>()
    private val _isAdmin = MutableLiveData<Boolean>()

    val getResponse: LiveData<Response<GameRoomRetro>>
        get() = _getResponse

    val getResponses: LiveData<List<GameRoom>>
        get() = _getResponses

    val insertResponse: LiveData<GameRoom>
        get() = _insertResponse

    val getIsAdmin:MutableLiveData<Boolean>
        get() = _isAdmin

    val addRoomResponse:LiveData<Boolean>
        get() = _addRoomResponse

    val chosenCategory:LiveData<Category>
        get() = _chosenCategory

    @Bindable
    fun getAd():Boolean {
        return getIsAdmin.value!!
    }

    @Bindable
    val price = MutableLiveData<String>()

    @Bindable
    val roomName = MutableLiveData<String>()



//    fun getAllRooms() = viewModelScope.launch(Dispatchers.IO) {
////         roomRepository.getAllRoomsNet()
//        _getResponses.postValue(roomRepository.getAllRoomsNet())
//    }

    fun getAllActiveRooms() = viewModelScope.launch{
        roomRepository.getAllActiveRoomsNet().observeForever {
            _getResponses.postValue(it)
        }
    }

    fun getRoomsByCategory(category:Category) = viewModelScope.launch {

        roomRepository.getRoomsByCategory(category).observeForever {
            _getResponses.postValue(it)
        }
    }

    fun insertRoom(gameRoom: GameRoomRetro) = viewModelScope.launch{
        roomRepository.insertRoom(gameRoom).observeForever {
            _insertResponse.postValue(it)
        }
    }

    fun getRoomById(gameRoomId:Long) : LiveData<GameRoom>{
        return roomRepository.getRoomById(gameRoomId)
    }

    fun getRoom(id: Long): LiveData<GameRoom>{
        return  roomRepository.getRoomById(1L)
    }

    fun onGameItemClicked(id: Long){
//        Log.d("ROOM_TAG", "Game Item Clicked with ID $id")
//        findNa().navigate(R.id.gameplay_des, null)
//        return 1
    }

    fun onAddRoomClicked(){

        lateinit var category: Category

        _chosenCategory.observeForever {
            category = it
        }
        val room = readRoom(category)
        insertRoom(room)
    }


    fun setAdmin(){
        _isAdmin.value=true
    }

    fun setNormalUser(){
        _isAdmin.value=false
    }

    fun onAddFabClick(){
        _addRoomResponse.value = true
    }

    fun readRoom(category: Category) = GameRoomRetro (
        0,
        Date(),
        GameRoom.Type.ACTIVE,
        category,
        price.value.toString().toInt(),
        roomName.value.toString()
        )

    fun setCategory(category: Category){
        _chosenCategory.value = category
    }


}