package et.edu.aait.student.planetsimulation.dinero.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.adapter.GameAdapter
import et.edu.aait.student.planetsimulation.dinero.adapter.GameRoomAdapter
import et.edu.aait.student.planetsimulation.dinero.data.api.GameRoomRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentHomeBinding
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import et.edu.aait.student.planetsimulation.dinero.viewmodel.RoomViewModel
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel
import java.net.SocketTimeoutException

class HomeFragment : Fragment() {
    lateinit var listener: OnItemClickedListener
    lateinit var roomViewModel: RoomViewModel
    lateinit var userViewModel: UserViewModel
    private lateinit var homeRecyclerView: RecyclerView
    lateinit var gameRoomRetros: List<GameRoomRetro>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnItemClickedListener){
            listener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentHomeBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false)

        roomViewModel = ViewModelProviders.of(this).get(RoomViewModel::class.java)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        binding.roomViewModel = roomViewModel
        binding.userViewModel = userViewModel
        binding.userCredit = ""+0

        val gameRoomAdapter = GameRoomAdapter(roomViewModel, object :
            OnItemClickedListener{
            override fun onGameItemClicked(gameRoom: GameRoom) {
                val args = Bundle()
                args.putSerializable("gameRoom", gameRoom)
                userViewModel.getUser().observe(viewLifecycleOwner, Observer{
                    userViewModel.getResponse.observe(viewLifecycleOwner, Observer {
                        response ->
                            response.body().run {
                                if(this != null && this.credit - gameRoom.price >= 0){
                                    val currCred = this.credit - gameRoom.price
                                    this.credit = currCred
                                    userViewModel.updateUserNetAsync(this)
                                    findNavController().navigate(R.id.gameplay_des)
                                }
                                else{
                                    Toast.makeText(context, "Buy More credits", Toast.LENGTH_SHORT).show()
                                }
                            }
                    })
                })
            }
            }
        );

        roomViewModel.getAllActiveRooms()
        roomViewModel.getResponses.observe(viewLifecycleOwner, Observer {
            response -> gameRoomAdapter.submitList(response)
        })

        binding.roomsRecyclerView.adapter = gameRoomAdapter

        var i = true
        userViewModel.getUser().observe(this, Observer {
            //                Log.d("GAMEROOM_TAG", "${it.firstName}")
            if(i){
                userViewModel.getUserByEmailNet(it.email)
                userViewModel.getResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        binding.userCredit = "" + this.credit
                        binding.executePendingBindings()
                    }
                })
                i = false
            }
        })



        binding.setLifecycleOwner{ lifecycle }

        return binding.root
    }

    interface OnItemClickedListener{
        fun onGameItemClicked(gameRoom: GameRoom)
    }

}