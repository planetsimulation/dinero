package et.edu.aait.student.planetsimulation.dinero.data.api

import et.edu.aait.student.planetsimulation.dinero.data.local.Answer
import java.io.Serializable
import java.util.*

data class AnswerRetro(
    val id:Long,
    val createdAt: Date?,
    val content: String,
    val description:String
):Serializable{

    fun convertToAnswer():Answer{

        val answer = Answer(this.id,this.createdAt,this.content,this.description)
        return answer
    }
}