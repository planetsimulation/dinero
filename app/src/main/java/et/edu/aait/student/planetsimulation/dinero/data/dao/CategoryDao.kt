package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.Category

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: Category):Long

    @Update
    fun updateCategory(category: Category):Int

    @Delete
    fun deleteCategory(category: Category):Int

    @Query("SELECT * FROM categories WHERE categories.id= :id LIMIT 1")
    fun getCategoryById(id:Long): LiveData<Category>

    @Query("SELECT * FROM categories")
    fun getCategory():LiveData<List<Category>>


}