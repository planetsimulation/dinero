package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.room.Room
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.*
import et.edu.aait.student.planetsimulation.dinero.data.local.*
import et.edu.aait.student.planetsimulation.dinero.network.*
import et.edu.aait.student.planetsimulation.dinero.repository.AnswerRepository
import et.edu.aait.student.planetsimulation.dinero.repository.CategoryRepository
import et.edu.aait.student.planetsimulation.dinero.repository.ChoiceRepository
import et.edu.aait.student.planetsimulation.dinero.repository.QuestionRepository
import kotlinx.coroutines.launch
import retrofit2.Response
import java.util.*

class AddQuestionViewModel(application: Application):AndroidViewModel(application),Observable {
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    var questionService: QuestionService
    var questionRepository: QuestionRepository

    var categoryService: CategoryService
    var categoryRepository: CategoryRepository


    var answerRepository:AnswerRepository
    var answerService:AnswerService

    var choiceRepository:ChoiceRepository
    var choiceService:ChoiceService

    init {
        val database= DineroDatabase.getDatabase(application)
        val questionDao=database.questionDao()
        val answerDao= database.answerDao()
        val choiceDao = database.choiceDao()
        val categoryDao = database.categoryDao()

        questionService= QuestionService.getInstance()
        questionRepository= QuestionRepository(application,questionDao,questionService)

        answerService = AnswerService.getInstance()
        answerRepository = AnswerRepository(application,answerDao,answerService)

        choiceService = ChoiceService.getInstance()
        choiceRepository = ChoiceRepository(application,choiceDao,choiceService)

        categoryService = CategoryService.getInstance()
        categoryRepository = CategoryRepository(application,categoryDao,categoryService)
    }

    private val _getCategoryResponse = MutableLiveData<Category>()
    private val _getQuestionsResponse = MutableLiveData<List<Question>>()
    private val _insertAnswerResponse = MutableLiveData<Answer>()
    private val _insertQuestionResponse = MutableLiveData<Question>()
    private val _insertChoiceResponse = MutableLiveData<Choice>()
    private val _chosenCategory = MutableLiveData<Category>()
    private val _chosenRoom = MutableLiveData<GameRoom>()
    private val _addQuestionResponse = MutableLiveData<Boolean>()


    @Bindable
    val questionContent = MutableLiveData<String>()

    @Bindable
    val questionXp = MutableLiveData<String>()

    @Bindable
    val answerContent = MutableLiveData<String>()

    @Bindable
    val firstChoiceContent = MutableLiveData<String>()

    @Bindable
    val secondChoiceContent = MutableLiveData<String>()

    @Bindable
    val thirdChoiceContent = MutableLiveData<String>()


    val getCategoryResponse: LiveData<Category>
        get() = _getCategoryResponse
    val insertAnswerResponse: LiveData<Answer>
        get() = _insertAnswerResponse
    val insertQuestionResponse: LiveData<Question>
        get() = _insertQuestionResponse
    val insertChoiceResponse: LiveData<Choice>
        get() = _insertChoiceResponse
    val getQuestionsResponse:LiveData<List<Question>>
        get() = _getQuestionsResponse

    val chosenRoom:LiveData<GameRoom>
        get() = _chosenRoom

    val chosenCategory:LiveData<Category>
        get() = _chosenCategory

    val addQuestionResponse: LiveData<Boolean>
    get() = _addQuestionResponse




    fun getCategoryByIdNet(categoryId: Long)= viewModelScope.launch{
        categoryRepository.getCategoryByIdNet(categoryId).observeForever{
            _getCategoryResponse.postValue(it)
        }
    }

    fun insertAnswerNet(answerRetro: AnswerRetro)= viewModelScope.launch {
        answerRepository.insertAnswerNet(answerRetro).observeForever{
            _insertAnswerResponse.postValue(it)
        }
    }

    fun insertChoiceNet(choiceRetro: ChoiceRetro)= viewModelScope.launch {
        choiceRepository.insertChoiceNet(choiceRetro).observeForever{
            _insertChoiceResponse.postValue(it)
        }
    }

    fun insertQuestionNet(questionRetro: QuestionRetro)= viewModelScope.launch {
        questionRepository.insertQuestionNet(questionRetro).observeForever{
            _insertQuestionResponse.postValue(it)
        }
    }

    fun getQuestions() = viewModelScope.launch {
        questionRepository.getQuestionNet().observeForever {
            _getQuestionsResponse.postValue(it)
        }
    }

    fun onAddQuestionButtonClick(){

        lateinit var answer:AnswerRetro
        lateinit var category:Category
        lateinit var choice:ChoiceRetro
        lateinit var room:GameRoom


        if(Utility.checkConnection(getApplication())){
            this.insertAnswerNet(readAnswer())
            this.insertAnswerResponse.observeForever(androidx.lifecycle.Observer { response->
                response?.run {
                    answer= this.convertToAnswerRetro()

                    insertChoiceNet(readChoices())
                    insertChoiceResponse.observeForever(androidx.lifecycle.Observer { response ->
                        response?.run {
                            choice = this.convertToChoiceRetro()

                            Log.d("inserted answer",answer.id.toString())

                            _chosenCategory.observeForever {
                                category = it
                            }

                            _chosenRoom.observeForever {
                                room = it
                            }

                            val gameRoomRetro= GameRoomRetro(room.id,room.createdAt,GameRoom.Type.ACTIVE,category,room.price,room.roomName)
                            insertQuestionNet(readQuestion(answer,choice,gameRoomRetro))
                            insertQuestionResponse.observeForever(androidx.lifecycle.Observer { response ->
                                response?.run {
                                    Log.d("inserted Quesion",this.toString())
                                }
                            })
                        }


                    })
                }




            })
        }

        _addQuestionResponse.value = true


    }

    private fun readAnswer()= AnswerRetro(
        0, Date(), answerContent.value.toString(),answerContent.value.toString()
    )

    private fun readChoices() = ChoiceRetro(
        0,
        answerContent.value.toString(),
        firstChoiceContent.value.toString(),
        secondChoiceContent.value.toString(),
        thirdChoiceContent.value.toString()
    )

    private fun readQuestion(answerRetro: AnswerRetro,choiceRetro: ChoiceRetro,gameRoomRetro: GameRoomRetro) = QuestionRetro(
        0 ,
        Date() ,
        questionContent.value.toString(),
        questionXp.value.toString().toInt(),
        answerRetro,
        gameRoomRetro,
        choiceRetro

    )

    fun setCategory(category: Category){
        _chosenCategory.value = category
    }

    fun setGameRoom(gameRoom: GameRoom){
        _chosenRoom.value = gameRoom
    }



}