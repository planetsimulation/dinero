package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom

@Dao
interface GameRoomDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoom(room: GameRoom):Long

    @Query("SELECT * FROM gameRoom WHERE gameRoom.id= :id LIMIT 1")
    fun getRoomById(id: Long):LiveData<GameRoom>

    @Query("SELECT * FROM gameRoom")
    fun getRooms():LiveData<List<GameRoom>>
}