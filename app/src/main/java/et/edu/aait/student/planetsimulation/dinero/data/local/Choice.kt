package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import et.edu.aait.student.planetsimulation.dinero.data.api.ChoiceRetro
import java.io.Serializable
import java.util.*

@Entity(tableName = "choices")
data class Choice(
    @PrimaryKey @ColumnInfo(name="id") var id:Long,
    @ColumnInfo(name = "first_choice") val firstChoice: String,
    @ColumnInfo(name= "second_choice") val secondChoice: String,
    @ColumnInfo(name= "third_choice") val thirdChoice:String,
    @ColumnInfo(name= "fourth_choice") val fourthChoice:String
    ):Serializable{

    fun convertToChoiceRetro():ChoiceRetro{

        return ChoiceRetro(this.id,this.firstChoice,this.secondChoice,this.thirdChoice,this.fourthChoice)
    }
}