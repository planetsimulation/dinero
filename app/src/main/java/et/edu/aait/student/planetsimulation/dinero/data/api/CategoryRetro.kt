package et.edu.aait.student.planetsimulation.dinero.data.api

import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import java.io.Serializable
import java.util.*

data class CategoryRetro(
    val id:Long,
    val createdAt: Date?,
    val name:String,
    val details:String
):Serializable{

    fun convertToCategory():Category{

        val category = Category(this.id,this.createdAt,this.name,this.details)
        return category
    }
}