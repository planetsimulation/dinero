package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.util.Log
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.HistoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.network.HistoryService
import et.edu.aait.student.planetsimulation.dinero.repository.HistoryRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class HistoryViewModel(application: Application): AndroidViewModel(application), Observable {
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

//    private val _getResponse = MutableLiveData<Response<CategoryRetro>>()


    lateinit var historyRepository: HistoryRepository
    lateinit var historyService: HistoryService

    init {
        val database = DineroDatabase.getDatabase(application)
        val historyDao = database.historyDao()
        historyService = HistoryService.getInstance()
        historyRepository = HistoryRepository(application, historyDao, historyService)
    }

    private val _getResponses= MutableLiveData<List<History>>()
    val getResponses:MutableLiveData<List<History>>
        get() = _getResponses

    private val _insertResponse= MutableLiveData<History>()
    val insertResponse: MutableLiveData<History>
        get() = _insertResponse


    fun getHistories(user: User)= viewModelScope.launch {
        historyRepository.getHistories(user).observeForever{
            _getResponses.postValue(it)
            Log.d("sixeeeee ","=="+it.size.toString()+"---"+user.id)
        }
    }

    fun insertHistory(history: HistoryRetro)= viewModelScope.launch {
        historyRepository.insertHistoryNet(history).observeForever{
            _insertResponse.postValue(it)
        }

    }
}