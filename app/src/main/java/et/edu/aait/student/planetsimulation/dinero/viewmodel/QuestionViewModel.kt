package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.QuestionRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Question
import et.edu.aait.student.planetsimulation.dinero.network.QuestionService
import et.edu.aait.student.planetsimulation.dinero.repository.QuestionRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class QuestionViewModel(application: Application):AndroidViewModel(application){

    var questionService:QuestionService
    var questionRepository:QuestionRepository

    init {
        val database= DineroDatabase.getDatabase(application)
        val questionDao=database.questionDao()

        questionService= QuestionService.getInstance()
        questionRepository= QuestionRepository(application,questionDao,questionService)
    }

    private val _getResponse = MutableLiveData<Response<QuestionRetro>>()
    private val _getResponses= MutableLiveData<Response<List<QuestionRetro>>>()

    val getResponse: LiveData<Response<QuestionRetro>>
        get() = _getResponse

    val getResponses: LiveData<Response<List<QuestionRetro>>>
        get() = _getResponses

//    fun insertQuestion(question: Question)= viewModelScope.launch(Dispatchers.IO){
//        questionRepository.insertQuestion(question)
//    }
//
//    fun updateQuestion(question: Question){
//        questionRepository.updateQuestion(question)
//    }
//
//    fun deleteQuestion(question: Question){
//        questionRepository.deleteQuestion(question)
//    }
//
//    fun getQuestionById(questionId:Int): LiveData<Question> {
//        return questionRepository.getQuestionById(questionId)
//    }
//
//    fun getQuestionByIdNet(questionId: Int): Deferred<Response<QuestionRetro>> {
//        return questionRepository.getQuestionByIdNet(questionId)
//    }
//
    fun getQuestions()= viewModelScope.launch(Dispatchers.IO){
        _getResponses.postValue(questionRepository.getQuestions())
    }
//
////    fun insertQuestionNet(questionRetro: QuestionRetro): Deferred<Response<QuestionRetro>> {
////        return questionRepository.insertQuestionNetAsync(questionRetro)
////    }
////
////    fun insertQuestionNetAsync(questionRetro: QuestionRetro): Deferred<Response<QuestionRetro>> {
////        return questionRepository.insertQuestionNetAsync(questionRetro)
////    }
}
