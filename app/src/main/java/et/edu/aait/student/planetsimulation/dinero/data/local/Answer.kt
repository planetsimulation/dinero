package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import et.edu.aait.student.planetsimulation.dinero.data.api.AnswerRetro
import java.io.Serializable
import java.util.*

@Entity(tableName="answers")
data class Answer(
    @PrimaryKey @ColumnInfo(name = "id") var id:Long,
    @ColumnInfo(name = "created_at") val createdAt: Date?,
    @ColumnInfo(name = "content") val content:String,
    @ColumnInfo(name = "description") val description:String
    ):Serializable{

    fun convertToAnswerRetro():AnswerRetro{

        return AnswerRetro(this.id,this.createdAt,this.content,this.description)
    }
}