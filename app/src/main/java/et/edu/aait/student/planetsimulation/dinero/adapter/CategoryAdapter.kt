package et.edu.aait.student.planetsimulation.dinero.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.databinding.CategoryRecyclerViewItemBinding

class CategoryAdapter(onCategoryClickListener:OnCategoryClickListener):ListAdapter<Category, CategoryAdapter.ViewHolder>(CategoryDiffCallBack()){
    private val onClick= onCategoryClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item,onClick)
    }



    class ViewHolder private constructor(val binding: CategoryRecyclerViewItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(item: Category, onClick:OnCategoryClickListener){
            binding.category = item
            binding.root.setOnClickListener {
                onClick.onCategoryClick(item)
            }
            binding.executePendingBindings()
        }

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CategoryRecyclerViewItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class CategoryDiffCallBack: DiffUtil.ItemCallback<Category>(){
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem == newItem
    }

}

interface OnCategoryClickListener{
    fun onCategoryClick(category:Category)
}