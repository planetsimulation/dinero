package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel
import androidx.lifecycle.Observer
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {

    lateinit var userViewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel.getUser().observe(this, Observer {
            if(it != null){
                if(it.role==1){
                    findNavController().navigate(R.id.category_des,null)
                }
                else{
                    findNavController().navigate(R.id.home_des, null)
                }

            }
        })

        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(inflater,
            R.layout.fragment_login, container, false)

        userViewModel.loginResponse.observe(this, Observer { bool->
            if(bool){
                userViewModel.isAdmin.observe(this,Observer{admin->
                    if(admin){
                        findNavController().navigate(R.id.category_des,null)
                    }
                    else{
                        findNavController().navigate(R.id.home_des,null)
                    }
                })
            }
        })

        userViewModel.signupResponse.observe(this, Observer { bool->
            if(bool){
                findNavController().navigate(R.id.signup_des,null)
            }
        })


        binding.userViewModel = userViewModel

        return binding.root

    }


}