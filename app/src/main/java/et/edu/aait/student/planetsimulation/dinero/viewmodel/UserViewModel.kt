package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.core.app.AppLaunchChecker
import androidx.databinding.Bindable
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentLoginBinding
import et.edu.aait.student.planetsimulation.dinero.network.UserService
import et.edu.aait.student.planetsimulation.dinero.repository.UserRepository
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

class UserViewModel(application: Application):AndroidViewModel(application),Observable {

    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    var repository: UserRepository
    var service: UserService
    val loggedInUser :LiveData<User>
    var name: String = "1"
    var credit: String = "0"
    init {
        val database = DineroDatabase.getDatabase(application)
        val userDao = database.userDao()
        service = UserService.getInstance()
        repository = UserRepository(userDao, service)
        loggedInUser= repository.getUser()


    }

    private val _getResponse = MutableLiveData<Response<User>>()
    private val _getResponses = MutableLiveData<List<Response<User>>>()

    private val _updateResponse = MutableLiveData<Response<User>>()
    private val _updateResponses = MutableLiveData<List<Response<User>>>()

    private val _loginResponse = MutableLiveData<Boolean>()
    private val _signupResponse = MutableLiveData<Boolean>()
    private val _logoutResponse = MutableLiveData<Boolean>()
    private val _isAdmin = MutableLiveData<Boolean>()



    @Bindable
    val userEmail = MutableLiveData<String>()

    @Bindable
    val userPassword = MutableLiveData<String>()

    @Bindable
    val userFirstName = MutableLiveData<String>()

    @Bindable
    val userLastName = MutableLiveData<String>()

    val getResponse: LiveData<Response<User>>
        get() = _getResponse

    val getResponses: LiveData<List<Response<User>>>
        get() = _getResponses

    val updateResponse : LiveData<Response<User>>
        get() = _updateResponse

    val updateResponses : LiveData<List<Response<User>>>
        get() = _updateResponses

    val loginResponse : LiveData<Boolean>
        get() = _loginResponse

    val signupResponse: LiveData<Boolean>
        get() = _signupResponse

    val isAdmin: LiveData<Boolean>
        get() = _isAdmin


    val logoutResponse : LiveData<Boolean>
        get() = _logoutResponse


    fun insertUser(user: User) = viewModelScope.launch(Dispatchers.IO){
        repository.insertUser(user)
    }

    fun updateUser(user: User?) = viewModelScope.launch(Dispatchers.IO){
        repository.updateUser(user)
    }

    fun deleteUser(user: User?){
        repository.deleteUser(user)
    }

    fun getUserById(user_id: Int):LiveData<User>{
        return repository.getUserById(user_id)
    }

    fun getUser():LiveData<User>{
        return repository.getUser()
    }

    fun getUserByEmailNet(email: String) = viewModelScope.launch(Dispatchers.IO) {
        updateUser(repository.getUserByEmailNet(email).body())
        _getResponse.postValue(repository.getUserByEmailNet(email))
    }

    fun addUserNet(user: User):Deferred<Response<User>>{
        Log.d("SIGNUP_TAG", "in addUserNet "+user.email)
        return repository.addUserNet(user)
    }
    fun updateUserNetAsync(user: User) :Deferred<Response<User>>{
//        _updateResponse.postValue(repository.updateUserNetAsync(user))
        return repository.updateUserNetAsync(user)
    }

    fun makePayment(user: User):Int{
        Log.d("PAYMENT_TAG", "In Make Payment UserViewModel")
//        val user = repository.getUser()
//        Toast.makeText(get)
//        repository.updateUser(user.value)
        return 0
//        return 0
    }

    fun onLoginButtonClick(){
        val email = userEmail.value.toString()

        val password = userPassword.value.toString()

        _loginResponse.value = false
        getUserByEmailNet(email)
        getResponse.observeForever(Observer { response ->
            response.body()?.run {
                if(this.password.equals(password)){
                    if(this.role==1){
                        _isAdmin.value = true
                    }
                    _loginResponse.value = true

                    var user = User(this.id, Date(),this.email,this.firstName,this.lastName,this.password,this.xp,this.credit,this.role)
//                    credit = user.credit
                    insertUser(user)

                }
            }
        })

    }

    fun goToSignupButtonClick(){
        _signupResponse.value = true
    }

    fun onSignupButtonClick(){
        val email = userEmail.value.toString()
        Log.d("email is ",email);
        val firstName = userFirstName.value.toString()
        val lastName = userLastName.value.toString()
        val password = userPassword.value.toString()
        var user = User(0,Date(),email,firstName,lastName,password,0,0,2)
        addUserNet(user)
    }

    fun onLogOutButtonClick(){
        var x = false;
        getUser().observeForever{
            if(it != null) {
                GlobalScope.launch(Dispatchers.IO) {
                    deleteUser(it)
                }
                _logoutResponse.value = true
            }
//
        }


    }
}