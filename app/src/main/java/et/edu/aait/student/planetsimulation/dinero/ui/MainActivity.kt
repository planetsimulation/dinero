package et.edu.aait.student.planetsimulation.dinero.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import et.edu.aait.student.planetsimulation.dinero.R
//import et.edu.aait.student.planetsimulation.dinero.data.api
import et.edu.aait.student.planetsimulation.dinero.data.local.Pay
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel

class MainActivity : AppCompatActivity(){


    lateinit var userViewModel: UserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


        val host: NavHostFragment = supportFragmentManager
           .findFragmentById(R.id.main_frame) as NavHostFragment? ?: return


        val navController= host.navController

        setupBottomNavMenu(navController)
    }

    fun paymentClicked(pay: Pay) {
        Toast.makeText(this, "You clicked "+pay.title, Toast.LENGTH_SHORT).show()
    }


    private fun setupBottomNavMenu(navController: NavController) {
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNav?.setupWithNavController(navController)
    }



    }
