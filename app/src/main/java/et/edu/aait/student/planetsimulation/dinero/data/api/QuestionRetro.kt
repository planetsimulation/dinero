package et.edu.aait.student.planetsimulation.dinero.data.api

import com.squareup.moshi.Json
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.data.local.Question
import java.io.Serializable
import java.util.*

data class QuestionRetro(
    val id:Long,
    val createdAt: Date?,
    @Json(name = "content")
    val content:String,
    val xp:Int,
    val answer: AnswerRetro,
    val room: GameRoomRetro,
    val choice: ChoiceRetro
):Serializable {

    fun convertToQuestion(): Question {

        val question = Question(
            this.id,this.createdAt,this.content,this.xp,this.answer.id,this.room.id,this.choice.id)

        return question
    }
}