package et.edu.aait.student.planetsimulation.dinero.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*
import java.util.*


interface UserService {

    @GET("user/one/{email}")
    fun getUserAsync(@Path("email") email: String): Deferred<Response<User>>

    @POST("user")
    fun addUserAsync(@Body user: User): Deferred<Response<User>>

    @PATCH("user/update")
    fun updateUserAsync(@Body user: User): Deferred<Response<User>>

    @DELETE("user/{id}")
    fun deleteUserAsync(@Path("id") id:Long):Deferred<Response<Void>>


    companion object{
        val baseUrl = Utility.BASE_URL
        fun getInstance(): UserService{
            val moshi = Moshi.Builder()
                .add(Date::class.java!!, Rfc3339DateJsonAdapter().nullSafe())
                .build()
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(UserService::class.java)
        }
    }
}