package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.adapter.GameAdapter
import et.edu.aait.student.planetsimulation.dinero.adapter.GameRoomAdapter
import et.edu.aait.student.planetsimulation.dinero.adapter.OnGameClickListener
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.viewmodel.RoomViewModel
import et.edu.aait.student.planetsimulation.dinero.viewmodel.UserViewModel

class RoomListFragment : Fragment() {

    lateinit var roomViewModel: RoomViewModel
    lateinit var userViewModel: UserViewModel
    lateinit var loggedInUser: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: et.edu.aait.student.planetsimulation.dinero.databinding.FragmentRoomListBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_room_list,container,false)

        val category = arguments?.getSerializable("category") as Category

        roomViewModel = ViewModelProviders.of(this).get(RoomViewModel::class.java)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        roomViewModel.setAdmin()
        userViewModel.getUser().observe(this,Observer{user->
            loggedInUser = user
            Log.d("user role is ",user.role.toString())
            if (loggedInUser?.role!=1){
                roomViewModel.setNormalUser()
            }
        })

        binding.roomViewModel = roomViewModel

        val gameAdapter = GameAdapter(object :OnGameClickListener{
            override fun onGameClick(game: GameRoom) {
                val args = Bundle()
                args.putSerializable("game",game)
                args.putSerializable("category",category)
                if(loggedInUser?.role ==1){
                    findNavController().navigate(R.id.add_question_des,args)
                }
                else{
                    findNavController().navigate(R.id.gameplay_des,args)
                }

            }
        })
        binding.roomListRecyclerView.adapter = gameAdapter
        roomViewModel.getRoomsByCategory(category)
        Log.d("in room list","in list")
        roomViewModel.getResponses.observe(this, Observer { response->
            response?.run {
                gameAdapter.submitList(response)
            }
        })

        roomViewModel.addRoomResponse.observe(this, Observer {bool->
            if (bool){
                val ars = Bundle()
                ars.putSerializable("category",category)
                findNavController().navigate(R.id.add_room_des,ars)
            }
        })

        return binding.root
    }


}