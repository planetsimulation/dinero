package et.edu.aait.student.planetsimulation.dinero.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import et.edu.aait.student.planetsimulation.dinero.data.local.History
import et.edu.aait.student.planetsimulation.dinero.viewmodel.HistoryViewModel
import et.edu.aait.student.planetsimulation.dinero.databinding.HistoryRecyclerViewItemBinding

class HistoryAdapter(historyViewModel: HistoryViewModel):ListAdapter<History, HistoryAdapter.ViewHolder>(HistoryDiffCallBack()) {

    var historyViewModel: HistoryViewModel = historyViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
//        Log.d("HOME_LOG_TAG", "Item Id - "+item.id)
        holder.bind(item, this.historyViewModel)

    }

    class ViewHolder private constructor(val binding: HistoryRecyclerViewItemBinding) : RecyclerView.ViewHolder(binding.root){
        //        val gameTitle: TextView =binding.gameTitleTv
//        val gameDescription: TextView = binding.gameDescriptionTv
//        val gamePrice: TextView = binding.gamePriceTv
        fun bind(item: History, itemViewModel: HistoryViewModel){
            // bind item here
            binding.history = item
            binding.executePendingBindings()
        }

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = HistoryRecyclerViewItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

}

class HistoryDiffCallBack: DiffUtil.ItemCallback<History>(){
    override fun areItemsTheSame(oldItem: History, newItem: History): Boolean {
        return oldItem.id == newItem.id
    }
    override fun areContentsTheSame(oldItem: History, newItem: History): Boolean {
        return oldItem == newItem
    }
}