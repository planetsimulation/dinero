package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.AnswerRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Answer
import et.edu.aait.student.planetsimulation.dinero.network.AnswerService
import et.edu.aait.student.planetsimulation.dinero.repository.AnswerRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class AnswerViewModel(application: Application):AndroidViewModel(application) {

    var answerService: AnswerService
    var answerRepository: AnswerRepository

    init {
        val database= DineroDatabase.getDatabase(application)
        val answerDao=database.answerDao()

        answerService= AnswerService.getInstance()
        answerRepository= AnswerRepository(application,answerDao,answerService)
    }

    private val _getResponse = MutableLiveData<Answer>()
    private val _getResponses= MutableLiveData<List<Answer>>()
    private val _insertResponse = MutableLiveData<Answer>()
    private val _updateResponse = MutableLiveData<Answer>()

    val getResponse:LiveData<Answer>
        get() = _getResponse

    val getResponses:LiveData<List<Answer>>
        get() = _getResponses
    
    val insertResponse:LiveData<Answer>
        get() = _insertResponse
    
    val updateResponse:LiveData<Answer>
        get() = _updateResponse


    fun insertAnswer(answerRetro: AnswerRetro) = viewModelScope.launch{
        answerRepository.insertAnswerNet(answerRetro).observeForever{
            _insertResponse.postValue(it)
        }
    }

    fun getAnswerById(answerId: Long) = viewModelScope.launch {
        answerRepository.getAnswerByIdNet(answerId).observeForever{
            _getResponse.postValue(it)
        }
    }

    fun updateAnswer(answer: AnswerRetro) = viewModelScope.launch{
        answerRepository.updateAnswerNet(answer).observeForever{
            _updateResponse.postValue(it)
        }
    }

    fun deleteAnswer(answer: AnswerRetro)= viewModelScope.launch{
        answerRepository.deleteAnswerNet(answer)
    }
}