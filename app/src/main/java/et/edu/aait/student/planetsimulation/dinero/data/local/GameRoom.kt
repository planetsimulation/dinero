package et.edu.aait.student.planetsimulation.dinero.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "gameRoom",
    foreignKeys = arrayOf(ForeignKey(entity = Category::class,
                                        parentColumns = arrayOf("id"),
                                        childColumns = arrayOf("category_id"),
                                        onDelete = ForeignKey.CASCADE)))
data class GameRoom (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id : Long,
    @ColumnInfo(name = "created_at") val createdAt:Date,
    @ColumnInfo(name = "type") val typeId: Int,
    @ColumnInfo(name = "category_id") val categoryId: Long,
    @ColumnInfo(name = "price") val price: Int,
    @ColumnInfo(name = "room_name") val roomName: String
):Serializable{
    enum class Type{
        ACTIVE, INACTIVE, COMPLETE
    }
}
