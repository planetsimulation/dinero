package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentAddRoomBinding
import et.edu.aait.student.planetsimulation.dinero.viewmodel.RoomViewModel

class AddRoomFragment : Fragment() {


    lateinit var roomViewModel: RoomViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentAddRoomBinding>(inflater,R.layout.fragment_add_room,container,false)

        roomViewModel = ViewModelProviders.of(this).get(RoomViewModel::class.java)


        val category = arguments?.getSerializable("category") as Category

        roomViewModel.setCategory(category)



        binding.roomViewModel = roomViewModel

        return binding.root


    }


}