package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.data.dao.UserDao
import et.edu.aait.student.planetsimulation.dinero.network.UserService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class UserRepository(val application: Application, val userDao: UserDao, val userService: UserService) {

    suspend fun getUserByIdNet(email: String,userId:Long): LiveData<User> =
        withContext(Dispatchers.IO){
            if (Utility.checkConnection(application)){

                val response = userService.getUserAsync(email).await()
                val user =  response.body()
                if (user != null) {
                    userDao.insertUser(user)
                }
            }
            return@withContext userDao.getUserById(userId)
        }

    suspend fun insertUserNet(user : User ): LiveData<User> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                userService.addUserAsync(user ).await()
            }

            userDao.insertUser(user)
            return@withContext userDao.getUserById(user .id)
        }

    suspend fun updateUserNet(user : User ):LiveData<User> =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                userService.updateUserAsync(user )
            }

            userDao.updateUser(user)

            return@withContext userDao.getUserById(user .id)
        }

    suspend fun deleteUserNet(user : User ) =
        withContext(Dispatchers.IO){

            if(Utility.checkConnection(application)){
                userService.deleteUserAsync(user .id)
            }

            userDao.deleteUser(user)
        }

    suspend fun getUserByEmailNet(email: String): Response<User> =
        withContext(Dispatchers.IO) {
            userService.getUserAsync(email).await()
        }
}