package et.edu.aait.student.planetsimulation.dinero.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import et.edu.aait.student.planetsimulation.dinero.ui.HomeFragment
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.databinding.GameRecyclerViewItemBinding
import et.edu.aait.student.planetsimulation.dinero.viewmodel.RoomViewModel

class GameRoomAdapter(var roomViewModel: RoomViewModel, var listener: HomeFragment.OnItemClickedListener):ListAdapter<GameRoom, GameRoomAdapter.ViewHolder>(GameRoomDiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, this.roomViewModel, this.listener)
    }

    class ViewHolder private constructor(val binding: GameRecyclerViewItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: GameRoom, itemViewModel: RoomViewModel, listener: HomeFragment.OnItemClickedListener){
            // bind item here
            binding.gameRoom = item
            binding.gameRoomViewModel = itemViewModel
            binding.gameRoomItem.setOnClickListener{
                listener.onGameItemClicked(item)
            }

            binding.executePendingBindings()
        }

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GameRecyclerViewItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

}

class GameRoomDiffCallBack: DiffUtil.ItemCallback<GameRoom>(){
    override fun areItemsTheSame(oldItem: GameRoom, newItem: GameRoom): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GameRoom, newItem: GameRoom): Boolean {
        return oldItem == newItem
    }

}