package et.edu.aait.student.planetsimulation.dinero.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import et.edu.aait.student.planetsimulation.dinero.R
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import et.edu.aait.student.planetsimulation.dinero.databinding.FragmentAddCategoryBinding
import et.edu.aait.student.planetsimulation.dinero.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_add_category.view.*
import java.util.*

class AddCategoryFragment:Fragment() {

    lateinit var categoryViewModel: CategoryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentAddCategoryBinding>(
            inflater,
            R.layout.fragment_add_category,
            container,
            false
        )
        categoryViewModel =  ViewModelProviders.of(this).get(CategoryViewModel::class.java)

        categoryViewModel.insertResponse.observe(this,androidx.lifecycle.Observer { response ->
            if(response.id != null){
                findNavController().navigate(R.id.category_des,null)
            }
        })

        binding.categoryViewModel = categoryViewModel


        return binding.root
    }

    private fun readCategory(view: View)=CategoryRetro(
        0,
        Date(),
        view.category_name_et.text.toString(),
        view.category_description_et.text.toString()
    )
}