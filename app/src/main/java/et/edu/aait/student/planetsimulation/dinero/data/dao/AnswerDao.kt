package et.edu.aait.student.planetsimulation.dinero.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import et.edu.aait.student.planetsimulation.dinero.data.local.Answer

@Dao
interface AnswerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAnswer(answer: Answer):Long

    @Update
    fun updateAnswer(answer: Answer):Int

    @Delete
    fun deleteAnswer(answer: Answer):Int

    @Query("SELECT * FROM answers WHERE answers.id= :id LIMIT 1")
    fun getAnswerById(id:Long): LiveData<Answer>
}