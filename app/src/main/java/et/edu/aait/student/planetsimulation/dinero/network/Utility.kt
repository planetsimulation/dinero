package et.edu.aait.student.planetsimulation.dinero.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import et.edu.aait.student.planetsimulation.dinero.data.api.QuestionRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Question

class Utility {

    companion object{

        const val BASE_URL = "http://192.168.43.35:8080/"

        fun  checkConnection(context: Context?):Boolean{
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            return isConnected
        }

        var questionList: List<QuestionRetro>? = arrayListOf()
        var questionIndex: Int = 0
        var score: Int = 0
        var wrong: Int = 0
    }

}