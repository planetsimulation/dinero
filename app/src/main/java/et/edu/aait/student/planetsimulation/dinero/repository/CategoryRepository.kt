package et.edu.aait.student.planetsimulation.dinero.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.dao.CategoryDao
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.network.CategoryService
import et.edu.aait.student.planetsimulation.dinero.network.Utility
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class CategoryRepository(val application: Application,val categoryDao: CategoryDao, val categoryService: CategoryService) {

    //Do web calls if network available and then do local caching
    suspend fun getCategoryByIdNet(categoryId: Long): LiveData<Category> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){

                val response = categoryService.getCategoryAsync(categoryId).await()
                val category = response.body()
                if(category != null){
                    categoryDao.insertCategory(category.convertToCategory())
                }
            }
            return@withContext categoryDao.getCategoryById(categoryId)
    }

    suspend fun getCategoriesNet():LiveData<List<Category>> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                val response=categoryService.getCategoriesAsync().await()
                val categories = response.body()
                if (categories != null) {
                    categories.forEach {
                            category-> categoryDao.insertCategory(category.convertToCategory())

                    }

                }
            }
            return@withContext categoryDao.getCategory()
    }

    suspend fun insertCategoryNet(categoryRetro: CategoryRetro):LiveData<Category> =
        withContext(Dispatchers.IO){
            if(Utility.checkConnection(application)){
                categoryService.addCategoryAsync(categoryRetro).await()
            }

            categoryDao.insertCategory(categoryRetro.convertToCategory())
            return@withContext categoryDao.getCategoryById(categoryRetro.id)

    }

    suspend fun updateCategoryNet(categoryRetro: CategoryRetro):LiveData<Category> =
            withContext(Dispatchers.IO){

                if(Utility.checkConnection(application)){
                    categoryService.updateCategoryAsync(categoryRetro.id,categoryRetro)
                }

                categoryDao.updateCategory(categoryRetro.convertToCategory())

                return@withContext categoryDao.getCategoryById(categoryRetro.id)
            }

    suspend fun deleteCategoryNet(categoryRetro: CategoryRetro) =
            withContext(Dispatchers.IO){

                if(Utility.checkConnection(application)){
                    categoryService.deleteCategoryAsync(categoryRetro.id)
                }

                categoryDao.deleteCategory(categoryRetro.convertToCategory())
            }
}