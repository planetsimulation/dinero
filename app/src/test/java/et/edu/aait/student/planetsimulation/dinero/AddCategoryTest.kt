package et.edu.aait.student.planetsimulation.dinero

import androidx.navigation.NavController
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

class AddCategoryTest {
    @Test
    fun addCategory(){
        val navController = Mockito.mock(NavController::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        Espresso.onView(ViewMatchers.withId(R.id.email)).perform(ViewActions.typeText("admin@admin.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.login_password)).perform(ViewActions.typeText("password"))
        Espresso.onView(ViewMatchers.withId(R.id.login_btn)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.category_name_et)).perform(ViewActions.typeText("game title"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.category_description_et)).perform(ViewActions.typeText("This is a description of a game"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.add_category_button)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withText("game title")).check(ArgumentMatchers.matches(isDisplayed()))
    }
}