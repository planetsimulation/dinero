package et.edu.aait.student.planetsimulation.dinero

import androidx.navigation.NavController
import androidx.test.core.app.ActivityScenario
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity
import org.junit.Test

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.click


import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.espresso.Espresso.onView
import org.mockito.Mockito


class LoginTest {

    @Test
    fun loginUser(){
        val navController = Mockito.mock(NavController::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        onView(withId(R.id.email)).perform(typeText("title"), closeSoftKeyboard())
        onView(withId(R.id.login_password)).perform(typeText(""))
        onView(withId(R.id.login_btn)).perform(click())

        Mockito.verify(navController).navigate(R.id.home_fragment)
    }

}