package et.edu.aait.student.planetsimulation.dinero

import androidx.navigation.NavController
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import et.edu.aait.student.planetsimulation.dinero.repository.UserRepository
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

class AddUserTest {
    @Test
    fun addUser(){
        val navController = mock(NavController::class.java)
        launchFragment(navController)

        Espresso.onView(ViewMatchers.withId(R.id.signup_fname_value)).perform(ViewActions.typeText("fname"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_lname_value)).perform(ViewActions.typeText("lname"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_email_value)).perform(ViewActions.typeText("btw@email.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_phone_value)).perform(ViewActions.typeText("0909090909"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_password_value)).perform(ViewActions.typeText("password"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_confirm_password_value)).perform(ViewActions.typeText("password"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_button)).perform(ViewActions.click())

        val users = (UserRepository.getTasksBlocking(true) as Result.Success).data
        assertEquals(users[0].firstName,"fname")
        assertEquals(users[0].lastName,"lname")
        assertEquals(users[0].email,"btw@email.com")
        assertEquals(users[0].password,"password")
    }
}