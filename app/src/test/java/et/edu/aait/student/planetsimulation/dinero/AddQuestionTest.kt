package et.edu.aait.student.planetsimulation.dinero
import android.view.View
import androidx.navigation.NavController
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.Espresso
import androidx.test.core.app.ActivityScenario
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

class AddQuestionTest {
    @Test
    fun addQuestion(){
        val navController = Mockito.mock(NavController::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        Espresso.onView(ViewMatchers.withId(R.id.add_question_value)).perform(ViewActions.typeText("Question"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.correct_answer_value)).perform(ViewActions.typeText("Correct Answer"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.extra_answer_value1)).perform(ViewActions.typeText("First Choice"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.extra_answer_value2)).perform(ViewActions.typeText("Second Choice"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.extra_answer_value3)).perform(ViewActions.typeText("Third Choice"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.xp_value)).perform(ViewActions.typeText("1"),ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.save_question_button)).perform(ViewActions.click())
        Mockito.verify(navController).navigate(R.id.fragment_category)

    }
}