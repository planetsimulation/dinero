package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.local.GameRoom
import et.edu.aait.student.planetsimulation.dinero.network.RoomService
import et.edu.aait.student.planetsimulation.dinero.repository.RoomRepository
import org.hamcrest.CoreMatchers
import org.junit.*

import org.junit.Assert.*
import java.util.*

class RoomViewModelTest {

    private lateinit var roomViewModel: RoomViewModel
    private lateinit var dineroDatabase: DineroDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        dineroDatabase = Room.inMemoryDatabaseBuilder(context,DineroDatabase::class.java).build()

        val roomRepository = RoomRepository(dineroDatabase.roomDao(), RoomService.getInstance())
        roomViewModel = RoomViewModel(Application())
    }

    @After
    fun tearDown() {
        dineroDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun insertAndRetrieve(){
        val room = GameRoom(1, Date(), 0, 1, 2)
        roomViewModel.insertRoom(room)

        val result = roomViewModel.getRoomById(room.id)

        assertThat(result, CoreMatchers.notNullValue())
        assertThat(result.value?.id.toString(),CoreMatchers.`is`("1"))
        assertThat(result.value?.typeId,CoreMatchers.`is`(1))
        assertThat(result.value?.price,CoreMatchers.`is`(2))
    }
}