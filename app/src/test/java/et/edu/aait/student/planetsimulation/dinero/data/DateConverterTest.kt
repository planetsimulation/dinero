package et.edu.aait.student.planetsimulation.dinero.data

import androidx.databinding.adapters.Converters
import org.junit.Test

import org.junit.Assert.*
import java.util.*
import java.util.Calendar.*

class DateConverterTest {

    private val cal = Calendar.getInstance().apply {
        set(YEAR,2019)
        set(MONTH,JUNE)
        set(DAY_OF_MONTH,10)
    }

    @Test
    fun fromTimestamp() {
        assertEquals(cal.timeInMillis,DateConverter().fromTimestamp(cal.toString().toLong()))
    }

    @Test
    fun dateToTimestamp() {
        assertEquals(DateConverter().dateToTimestamp(cal.time),cal)
    }
}