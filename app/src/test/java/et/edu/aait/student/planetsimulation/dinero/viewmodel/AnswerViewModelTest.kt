package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.data.api.AnswerRetro
import et.edu.aait.student.planetsimulation.dinero.network.AnswerService
import et.edu.aait.student.planetsimulation.dinero.repository.AnswerRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import org.mockito.stubbing.Answer
import java.util.*
import kotlin.math.log

class AnswerViewModelTest {
    private lateinit var answerViewModel: AnswerViewModel
    private lateinit var dineroDatabase: DineroDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        dineroDatabase = Room.inMemoryDatabaseBuilder(context,DineroDatabase::class.java).build()

        val answerRepository = AnswerRepository(Application(),dineroDatabase.answerDao(), AnswerService.getInstance())
        answerViewModel = AnswerViewModel(Application())
    }

    @After
    fun tearDown(){
        dineroDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun insertAndRetrieve() = runBlocking {
        val answerRetro = AnswerRetro(1, Date(),"Content","Description")
        answerViewModel.insertAnswer(answerRetro)
        val result = answerViewModel.getAnswerById(answerRetro.id)
        answerViewModel.getResponse.observeForever{
            Assert.assertThat(it.id?.toString(),CoreMatchers.`is`("1"))
            Assert.assertThat(it.content.toString(),CoreMatchers.`is`("Content"))
            Assert.assertThat(it.description?.toString(),CoreMatchers.`is`("Description"))
        }
    }
}