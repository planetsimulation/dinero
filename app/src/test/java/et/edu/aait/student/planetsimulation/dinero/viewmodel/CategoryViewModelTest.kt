package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
//import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import et.edu.aait.student.planetsimulation.dinero.repository.CategoryRepository

import org.junit.Assert.*
import et.edu.aait.student.planetsimulation.dinero.data.api.CategoryRetro
import et.edu.aait.student.planetsimulation.dinero.data.local.Category
import et.edu.aait.student.planetsimulation.dinero.network.CategoryService
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import java.util.*


class CategoryViewModelTest{

    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var dineroDatabase: DineroDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        dineroDatabase = Room.inMemoryDatabaseBuilder(context,DineroDatabase::class.java).build()

        val categoryRepository = CategoryRepository(Application(),dineroDatabase.categoryDao(), CategoryService.getInstance())
        categoryViewModel= CategoryViewModel(Application())

    }

    @After
    fun tearDown(){
        dineroDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun insertAndRetrieve()= runBlocking {
        val category = CategoryRetro(1, Date(),"movie","description about movie")
        categoryViewModel.insertCategory(category)

        val result=categoryViewModel.getCategoryById(category.id)
        categoryViewModel.getResponse.observeForever {
            Assert.assertThat(result, CoreMatchers.notNullValue())
            Assert.assertThat(it.id?.toString(), CoreMatchers.`is`("1"))
            Assert.assertThat(it.name, CoreMatchers.`is`("movie"))
            Assert.assertThat(it.details, CoreMatchers.`is`("description about movie"))
        }

    }


}