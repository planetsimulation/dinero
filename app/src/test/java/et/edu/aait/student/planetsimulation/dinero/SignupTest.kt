package et.edu.aait.student.planetsimulation.dinero

import android.content.Context
import androidx.navigation.NavController
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class SignupTest {
    @Test
    fun signUpUser(){
        val navController = mock(NavController::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        Espresso.onView(ViewMatchers.withId(R.id.signup_fname_value)).perform(ViewActions.typeText("fname"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_lname_value)).perform(ViewActions.typeText("lname"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_email_value)).perform(ViewActions.typeText("btw@email.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_phone_value)).perform(ViewActions.typeText("0909090909"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_password_value)).perform(ViewActions.typeText("password"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_confirm_password_value)).perform(ViewActions.typeText("password"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.signup_button)).perform(ViewActions.click())

            verify(navController).navigate(R.id.home_fragment)
    }

}