package et.edu.aait.student.planetsimulation.dinero

import androidx.navigation.NavController
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.core.app.ActivityScenario
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity
import org.junit.Test
import org.hamcrest.Matcher
import androidx.test.espresso.Espresso
import org.mockito.Mockito

class CategoriesListFragment {
    @Test
    fun CategoriesList(){
        val navController = Mockito.mock(NavController::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        //Espresso.onView(ViewMatchers.withId(R.id.category_list_recycler_view)).check()
        Espresso.onView(ViewMatchers.withId(R.id.addFab)).perform(ViewActions.click())
        Mockito.verify(navController).navigate(R.id.fragment_add_category)

    }
}