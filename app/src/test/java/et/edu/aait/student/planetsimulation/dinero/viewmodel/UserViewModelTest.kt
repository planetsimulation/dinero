package et.edu.aait.student.planetsimulation.dinero.viewmodel

import android.app.Application
import et.edu.aait.student.planetsimulation.dinero.data.DineroDatabase
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import et.edu.aait.student.planetsimulation.dinero.data.local.User
import et.edu.aait.student.planetsimulation.dinero.network.CategoryService
import et.edu.aait.student.planetsimulation.dinero.network.UserService
import et.edu.aait.student.planetsimulation.dinero.repository.CategoryRepository
import et.edu.aait.student.planetsimulation.dinero.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import java.util.*

class UserViewModelTest {

    private lateinit var userViewModel: UserViewModel
    private lateinit var dineroDatabase: DineroDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        dineroDatabase = Room.inMemoryDatabaseBuilder(context,DineroDatabase::class.java).build()

        val userRepository = UserRepository(dineroDatabase.userDao(), UserService.getInstance())
        userViewModel = UserViewModel(Application())
    }

    @After
    fun tearDown(){
        dineroDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun insertAndRetrieve() = runBlocking {
        val user = User(1, Date(),"d@gmail.com","Dagmawit","Alemayehu","password",0,20,1)
        userViewModel.insertUser(user)

        val result = userViewModel.getUserById(user.id)

        Assert.assertThat(result.value?.id.toString(),CoreMatchers.`is`("1"))
        Assert.assertThat(result.value?.email.toString(),CoreMatchers.`is`("d@gmail.com"))
        Assert.assertThat(result.value?.firstName.toString(),CoreMatchers.`is`("Dagmawit"))
        Assert.assertThat(result.value?.lastName.toString(),CoreMatchers.`is`("Alemayehu"))
        Assert.assertThat(result.value?.password.toString(),CoreMatchers.`is`("password"))
        Assert.assertThat(result.value?.xp.toString(),CoreMatchers.`is`("0"))
        Assert.assertThat(result.value?.credit.toString(),CoreMatchers.`is`("20"))
        Assert.assertThat(result.value?.role.toString(),CoreMatchers.`is`("1"))
    }
}