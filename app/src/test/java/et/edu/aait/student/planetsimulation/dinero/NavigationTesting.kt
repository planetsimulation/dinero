package et.edu.aait.student.planetsimulation.dinero

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import et.edu.aait.student.planetsimulation.dinero.ui.LoginFragment
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class NavigationTesting {
    @Test
    fun navigationTestingLogin(){
        val scenario = launchFragmentInContainer<LoginFragment>(Bundle(), R.style.AppTheme)
        val navController = mock(NavController::class.java)
        scenario.onFragment{
            Navigation.setViewNavController(it.view!!, navController)
        }

        Espresso.onView(ViewMatchers.withId(R.id.email)).perform(ViewActions.typeText("title"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.login_password)).perform(ViewActions.typeText(""))
        Espresso.onView(ViewMatchers.withId(R.id.login_btn)).perform(ViewActions.click())

        verify(navController).navigate(R.id.home_fragment)

    }
}