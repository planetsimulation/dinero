package et.edu.aait.student.planetsimulation.dinero

import androidx.test.InstrumentationRegistry
import androidx.test.core.app.ActivityScenario
import androidx.test.runner.AndroidJUnit4
import et.edu.aait.student.planetsimulation.dinero.ui.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("et.edu.aait.student.planetsimulation.dinero", appContext.packageName)
    }

    @Test
    fun navigationTest(){
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

    }
}
